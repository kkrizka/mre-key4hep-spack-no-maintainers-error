# MRE Key4hep Spack No Maintainers Error

Add the key4hep-spack repository to the spack installation causes the following
error when trying to install or query any package.

```
$ spack spec cmake
==> Error: name 'maintainers' is not defined
```

This occurs with spack v0.19.1 and the latest `release` branch (ref `1fc911b`
at the time of writing) of `key4hep-spack`. It does not occur with an earlier
ref (`ec3ae6f`) of `key4hep-spack`.